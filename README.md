# AS Cli


## Usage

1. `go build -o bin/list_obj` to generate executable file 
2. `ln -Fs /Users/arthasjiang/Documents/bitbucket/cli/bin/list_obj /usr/local/bin/list_obj` to create softlink
3. go to project folder and run `list_obj file`


## Example

```bash
list_obj src/main/resources/db/migration/V4_cdf_ei_rep/R__4.1.1_Incremental_pfizerus_ei_rep_stage_tables.sql
```

```bash
enginedb
${enginedb}.SparkDSERun
${enginedb}.SparkDSERunAccount
${enginedb}.SparkDSERunAccountEnhancedInsight
${enginedb}.SparkDSERunConfigFactor
${enginedb}.SparkDSERunAccountRep
${enginedb}.DSEConfig
${enginedb}.Rep
copystormdb
${copystormdb}.Account
${copystormdb}.User
internal_obj
${stagedb}.AS_Proc_Create_EI_Stage_Tables
${stagedb}.AS_RPT_CDF_EI_Insights_stg
${stagedb}.AS_RPT_CDF_EI_Insights_Rep_stg
${stagedb}.AS_RPT_Temp_AppTracking_Insights
${stagedb}.AS_RPT_Temp_DSESuggestionLifecycle_arc
${stagedb}.AS_RPT_Temp_Dsl_Sdra_Sdraei
${stagedb}.AS_RPT_APT_Increment_Id_stg
${stagedb}.AS_RPT_APT_Increment_Id_pre_stg
${stagedb}.AS_RPT_APT_Temp_Increment_Id_stg
${stagedb}.AS_RPT_CDF_EI_Insights_Rep_Temp_stg
${stagedb}.AS_RPT_APT_Temp_Incremental
stagedb
${stagedb}
${stagedb}.AKT_AppTracking
${stagedb}.DSESuggestionLifecycle_arc
${stagedb}.AKT_Accounts
${stagedb}.AKT_RepLicense
${stagedb}.AKT_ProductCatalog
```
