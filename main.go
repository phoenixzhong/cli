package main

import (
  "os"
  "fmt"
  "log"
  "bufio"
  "regexp"
)

func append_if_not_exist(data []string, e string) []string {
  var found bool = false
  for _, v := range data {
    if v == e {
      found = true
      break
    }
  }

  if !found {
    data = append(data, e)
  }

  return data
}


func main() {
  file := os.Args[1]
  data := make(map[string] []string)
  data["stagedb"] = make([]string, 0)
  data["enginedb"] = make([]string, 0)
  data["copystormdb"] = make([]string, 0)
  data["internal_obj"] = make([]string, 0)

  f, err := os.Open(file)
  if err != nil {
    log.Fatal(err)
    return
  }
  defer f.Close()

  input := bufio.NewScanner(f)
  var line string
  var sanitize_obj string
  var re *regexp.Regexp
  var is_internal_obj bool

  for input.Scan() {
      line = input.Text()
      re = regexp.MustCompile(`\${`)
      obj_found := re.FindStringSubmatch(line)
      // handle objects when ${stagedb} ${enginedb} is found
      if len(obj_found) > 0 {
        // to match ${stagedb}.RPT_Suggestion_Delivered_stg
        re = regexp.MustCompile(`\${[^}]+}[^\s\(]+`)
        sanitize_obj = re.FindString(line)
        re = regexp.MustCompile(`[`+ "`" + `,;\(\)]`)
        sanitize_obj = re.ReplaceAllString(sanitize_obj, "")

        if regexp.MustCompile(`(?i) procedure`).Match([]byte(line)) || regexp.MustCompile(`(?i) table`).Match([]byte(line)) {

          data["internal_obj"] = append_if_not_exist(data["internal_obj"], sanitize_obj)
          continue
        }

        // skip if it's already marked as internal object
        is_internal_obj = false
        for _, e := range data["internal_obj"] {
          if e == sanitize_obj {
            is_internal_obj = true
            break
          }
        }

        if is_internal_obj {
          continue
        }

        if regexp.MustCompile(`stagedb`).Match([]byte(sanitize_obj)) {
          data["stagedb"] = append_if_not_exist(data["stagedb"], sanitize_obj)
        }
        if regexp.MustCompile(`enginedb`).Match([]byte(sanitize_obj)) {
          data["enginedb"] = append_if_not_exist(data["enginedb"], sanitize_obj)
        }
        if regexp.MustCompile(`copystormdb`).Match([]byte(sanitize_obj)) {
          data["copystormdb"] = append_if_not_exist(data["copystormdb"], sanitize_obj)
        }
      }
  }

  for k, v := range data {
    fmt.Println(k)
    for _, e := range v {
      fmt.Println(e)
    }
  }
}